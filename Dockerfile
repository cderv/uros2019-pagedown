FROM rocker/tidyverse:3.6.0

ARG CI_JOB_TOKEN

RUN apt-get update \
	&& apt-get install -y --no-install-recommends \
		wget \
		apt-transport-https \
		ca-certificates \
		gnupg \
	&& apt-get install -y \
	  libgconf-2-4 \
	&& wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add - \
	&& sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list' \
	&& apt-get update \
	&& apt-get install -y --no-install-recommends \
	  google-chrome-unstable \
	  fontconfig \
	  fonts-ipafont-gothic \
	  fonts-wqy-zenhei \
	  fonts-thai-tlwg \
	  fonts-kacst \
	  fonts-symbola \
	  fonts-noto \
	  fonts-freefont-ttf

RUN wget http://ftp.de.debian.org/debian/pool/contrib/m/msttcorefonts/ttf-mscorefonts-installer_3.6_all.deb -P . \
  && apt -y install ./ttf-mscorefonts-installer_3.6_all.deb \
  && rm -f ./ttf-mscorefonts-installer_3.6_all.deb

RUN rm /usr/local/bin/pandoc \
  && rm /usr/local/bin/pandoc-citeproc \
  && wget -O pandoc-2-6-1-amd64.deb https://github.com/jgm/pandoc/releases/download/2.6/pandoc-2.6-1-amd64.deb \
  && apt -y install ./pandoc-2-6-1-amd64.deb \
  && rm -f ./pandoc-2-6-1-amd64.deb

RUN rm -rf /var/lib/apt/lists/*

COPY . /usr/local/src/project

WORKDIR /usr/local/src/project

RUN install2.r --error remotes \
  && installGithub.r ropenscilabs/tic \
  && rm -rf /tmp/downloaded_packages/ /tmp/*.rds

RUN R -e 'tic::prepare_all_stages()' \
  && R -e 'tic::install()'
